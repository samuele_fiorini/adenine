------------------------------------
Adenine: Clustering Performance
------------------------------------
Index Name          |    Index Score
------------------------------------
ami                 |    0.0752
------------------------------------
ari                 |    0.0452
------------------------------------
completeness        |    0.091
------------------------------------
homogeneity         |    0.0807
------------------------------------
silhouette          |    0.109
------------------------------------
v_measure           |    0.0856
------------------------------------
