------------------------------------
Adenine: Clustering Performance
------------------------------------
Index Name          |    Index Score
------------------------------------
ami                 |    0.994
------------------------------------
ari                 |    0.997
------------------------------------
completeness        |    0.994
------------------------------------
homogeneity         |    0.994
------------------------------------
silhouette          |    0.501
------------------------------------
v_measure           |    0.994
------------------------------------
